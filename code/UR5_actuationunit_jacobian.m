function [fk, J] = UR5_actuationunit_jacobian
% LAB4JAC Utility to compute the Forward Kinematics and Jacobian of the
%         UR5; fk and J have symbolic variables for the joint angles
%         represented as t1, t2, t3, t4, t5, t6.
%   fk - 3x1 Forward Kinematics matrix that maps joint positions to
%        end-effector positions
%   J  - 6x6 Jacobian matrix that maps joint velocities to end-effecter
%        velocities; the first 3 rows represent the linear velocity
%        Jacobian; the last 3 rows represent the angular velocity Jacobian
%
% Robert Grupp - Lab 4, 530.646 Fall 2014
% modified by Farshid Alambeigi for the new actuation Unit- Spring 2017

% t1 - t6 represent theta 1 - theta 6 and are the joint values for the UR5
% t7 is the roll motion of the snake actuation unit
% They are "free" parameters
% d1, d4, d5, d6, d7, a2, a3 are "fixed" paramters corresponding to DH link
% lengths; real values should be subsituted in for these 
syms t1 t2 t3 t4 t5 t6 t7 d1 d4 d5 d6 d7 a2 a3 real;

% Real value substitution
d1 = 0.089159;
d2 = 0;
d3 = 0;
d4 = 0.10915;
d5 = 0.09465;
d6 = 0.0823;
d7 = 0;
a2 = -0.425;
a3 = -0.39225;

% The only alpha values (rotation about X) are pi/2 and -pi/2, the
% corresponding homogeneous matrices are shown below:
H_alpha1 = [1  0  0  0;
            0  0 -1  0;
            0  1  0  0;
            0  0  0  1];

H_alpha4 = H_alpha1;

H_alpha5 = [1  0  0  0;
            0  0  1  0;
            0 -1  0  0;
            0  0  0  1];
H_alpha6 = H_alpha1;

% Link to Link frame transformations
T_1_to_0 = Lab4RotZ(t1) * Lab4TransZ(d1) * H_alpha1;
T_2_to_1 = Lab4RotZ(t2) * Lab4TransX(a2);
T_3_to_2 = Lab4RotZ(t3) * Lab4TransX(a3);
T_4_to_3 = Lab4RotZ(t4) * Lab4TransZ(d4) * H_alpha4;
T_5_to_4 = Lab4RotZ(t5) * Lab4TransZ(d5) * H_alpha5;
T_6_to_5 = Lab4RotZ(t6) * Lab4TransZ(d6) * H_alpha6;
T_7_to_6 = Lab4RotZ(t7) * Lab4TransZ(d7);

% Frame transformations from each link to the base
T_2_to_0 = T_1_to_0 * T_2_to_1;
T_3_to_0 = T_2_to_0 * T_3_to_2;
T_4_to_0 = T_3_to_0 * T_4_to_3;
T_5_to_0 = T_4_to_0 * T_5_to_4;
T_6_to_0 = T_5_to_0 * T_6_to_5; % Transformation from UR5 end-effecter frame to the base
T_7_to_0 = T_6_to_0 * T_7_to_6; % Transformation from the snake base frame to the base
% Location of the the snake base with respect to the base
fk_H = T_7_to_0 * [0;0;0;1];
fk = simplify(fk_H(1:3,:));

% Linear velocity (Eq. 4.50 of Spong; page 131)
J_V = [diff(fk, t1) diff(fk, t2) diff(fk, t3) diff(fk, t4) diff(fk, t5) diff(fk, t6) diff(fk, t7)];

% Z-Axes of each frame with respect to the base;
% Each T_i_to_0(1:3,1:3) is also just the product of the rotation matrices
% T_i_to_0(1:3,1:3) = R_1_to_0 * ... * R_i-1_to_i-2 * R_i_to_i-1
z_1 = T_1_to_0(1:3,1:3) * [0;0;1];
z_2 = T_2_to_0(1:3,1:3) * [0;0;1];
z_3 = T_3_to_0(1:3,1:3) * [0;0;1];
z_4 = T_4_to_0(1:3,1:3) * [0;0;1];
z_5 = T_5_to_0(1:3,1:3) * [0;0;1];
z_6 = T_6_to_0(1:3,1:3) * [0;0;1];
z_7 = T_7_to_0(1:3,1:3) * [0;0;1];
% Angular velocity (Eq. 4.48 of Spong; page 131)
J_w = [z_1 z_2 z_3 z_4 z_5 z_6 z_7];

% Final Jacobian
J = simplify([J_V; J_w]);

end

function H = Lab4TransX(a)
    H = [1 0 0 a;
         0 1 0 0;
         0 0 1 0;
         0 0 0 1];
end

function H = Lab4TransZ(d)
    H = [1 0 0 0;
         0 1 0 0;
         0 0 1 d;
         0 0 0 1];
end

function H = Lab4RotZ(theta)
    H = [ cos(theta)  -sin(theta) 0 0;
         -sin(theta)   cos(theta) 0 0;
          0            0          1 0;
          0            0          0 1];
end

