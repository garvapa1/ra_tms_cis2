%% Ganesh Arvapalli
%% Tangent plane to surface calculation
clc;
clearvars;

% [x,y,z] = sphere(100);
% ptCloud = pointCloud(sphere(100));
stl2vrml('../models/Head/head.stl');
cloud = pointCloud(geo);
pcshow(cloud);


[fx, fy] = gradient(z);
% z = gradient(z);
% y = gradient(y);
% x = gradient(x);
hold on;
pcshow([x(:),y(:),z(:)]);
normals = pcnormals(ptCloud);
u = normals(1:10:end,1:10:end,1);
v = normals(1:10:end,1:10:end,2);
w = normals(1:10:end,1:10:end,3);
quiver3(x,y,z,u,v,w);
hold off;

title('Sphere with Default Color Map');
xlabel('X');
ylabel('Y');
zlabel('Z');

% points = randi(100, 1000, 3);
% pcshow(points);