%% Ganesh Arvapalli
%% Path planning algorithm

% Variable declarations
clc;
clearvars;

% Symbol declarations
syms t1 t2 t3 t4 t5 t6 t7 d1 d4 d5 d6 d7 a2 a3;

duration = 10;
step_size = 0.01;
current_t = rand(7, 1)*pi - pi/2;
[fk, J] = UR5_actuationunit_jacobian;
current_x = double(subs(fk, [t1, t2, t3, t4, t5, t6, t7], transpose(current_t)));
initial_x = current_x;
desired_x = current_x+[0.1; 0.1; 0.1];   % Try to make a straight line path
%%
% Calculate vector to next point
del_x = [desired_x - current_x; 0; 0; 0];

path = [current_x];
distance = [];
force = [];
K = eye(6);
% % Calculate Jacobian in real time (in case force adjustments necessary)
while norm(del_x) >= 0.005
    newJ = subs(J, [t1, t2, t3, t4, t5, t6, t7], transpose(current_t));
    % del_t = lsqlin(double(newJ), del_x, K, ones(6,7));
    del_t = lsqlin(double(newJ), del_x);
    current_t = current_t + del_t * step_size;
    current_x = double(subs(fk, [t1, t2, t3, t4, t5, t6, t7],...
        transpose(current_t)));
    % disp(current_x);        % Check that your x value is actually reaching the desired point
    path = [path, current_x];
    del_x = [desired_x - current_x; 0; 0; 0];
    distance = [distance; norm(del_x)];
    force = [force; norm(K*del_x)];
end

plot(distance);
title('Distance vs. Step #');
xlabel('Step #');
ylabel('Distance to Point');
figure;
plot(force);
title('Force vs. Step #');
xlabel('Step #');
ylabel('Force (dummy)');
return

hold on;
plot3(path(1,:,:),path(2,:,:),path(3,:,:), ':bs');
plot3(initial_x(1), initial_x(2), initial_x(3), '*r');
plot3(desired_x(1), desired_x(2), desired_x(3), '*g');
hold off;
% Single calculation of Jacobian prior to motion (linear motion)
% newJ = subs(J, [t1, t2, t3, t4, t5, t6, t7], transpose(current_t));
% del_t = double(newJ)\del_x; % Use lsqlin for constraints later
% 
% % Add del_t * step_size
% current_t = current_t + del_t;
% 
% current_x = double(subs(fk, [t1, t2, t3, t4, t5, t6, t7], transpose(current_t)));
% del_x = desired_x - current_x
% plot3([initial_x(1), current_x(1), desired_x(1)],[initial_x(2), current_x(2), desired_x(2)],[initial_x(3), current_x(3), desired_x(3)]);

