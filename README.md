# Robot Assisted Transcranial Magnetic Stimulation

The subjective visual vertical (SVV) is a measure of upright perception attributed to vestibular feedback. Differences in the SVV and the true earth-vertical could potentially be linked to activity of the posterior aspect of the supramarginal gyrus (SMGp), a region in the right hemisphere of the brain. To investigate the link between the SMGp and the SVV, transcranial magnetic stimulation (TMS) can be used to inhibit activity in localized regions of the brain. By studying the impact of this inhibition, an activity map can be generated to help identify specific areas within the SMGp that lead to the greatest impact on the SVV. However, there is a need for a tool that can automatically conduct TMS during SVV assessment in a grid-like fashion. For this reason, we developed a robotic tool that will apply TMS in precise positions specified by researchers following MRI scans. Our progress included building a custom tool that can fit over a UR5 robot and translate between specified points linearly with some limited force feedback. Eventually, we hope this tool will be used to help assess brain activity in other regions once grid location can be customized and TMS can be reliably adjusted in between measurement points.

### Folders

- code
- images
- models
- papers

/code contains all relevant code for generating the trajectory between points. /images contains images used during the course of this project. /models contains models of the tool holder and all assemblies. /papers contains papers and presentations I wrote during the course of CIS II.

### Prerequisites

You will need Ubuntu 14.04, MATLAB R2016a+, 3D Slicer, CMake, and SolidWorks or Creo.
I ran Ubuntu 16.04 off a hard drive with satisfactory results, but installation was made much more difficult. Stick with 14.04 for development.
Do not use the 3D Slicer available on the website. Use the instructions provided to install with Debug mode.

### Installing

Installation for 3D Slicer can be somewhat difficult so here is a helpful link to [installation instructions](https://www.slicer.org/wiki/Documentation/Nightly/Developers/Build_Instructions). Follow these TO THE LETTER.
It may also be necessary to build the CISST-SAW packages: https://github.com/jhu-cisst/cisst-saw
The instructions to install those are [here](https://github.com/jhu-cisst/cisst/wiki/Compiling-cisst-and-SAW-with-CMake).

To get all files out of this repository, either download the zip file or clone the repository.

## Built With

* [MATLAB](https://www.mathworks.com/products/matlab.html) - For frame transformation and path planning
* [SolidWorks](https://www.solidworks.com/) - Building CAD models
* [3D Slicer](https://www.slicer.org/) - Used to create robot environment

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

I would like to thank Dr. Amir Kheradmand, Dr. Jorge Otero-Millan, Farshid Alambeigi, and Rachel Hegeman for their contributions towards the completion of this project. Additional thanks goes out to Dr. David S. Zee and Dr. Mehran Armand for guidance and for supporting the project. I'd also like to thank Dr. Russell Taylor and Ehsan Azimi for feedback and for running Computer Integrated Surgery II, through which this project was conducted.